<?php

/**
 * Implements hook_cashboard_api_project_get_project_id().
 */
function hook_cashboard_api_project_get_project_id($entity_type, $entity) {

  // If the nid is not set this is a node creation operation. The Value may
  // still be obtainable. This will happen during an ajax call in the
  // creation form.
  if (!isset($entity->nid)) {
    if (isset($entity->field_project[LANGUAGE_NONE][0]['target_id'])) {
      $project_source = $entity;
    }
    else {
      return FALSE;
    }
  }
  // If no comment but the project is set use the node for the project source.
  // This will happen during a node save.
  elseif (!isset($entity->cid) && isset($entity->field_project[LANGUAGE_NONE][0]['target_id'])) {
    $project_source = $entity;
  }
  // In clikcollab a comment is created upon node create.
  else {
    // Grab the latest comment and load it.
    $project_source = comment_load($entity->cid);

  }

  // Load the project for the latest comment.
  $project_node = node_load($project_source->field_project[LANGUAGE_NONE][0]['target_id']);

  // Find the project field for the project node and load the id.
  $field_list = cashboard_api_get_field_list('node', $project_node, 'cashboard_project');
  if (!empty($field_list)) {
    $field_name = array_shift($field_list);
    $project_id = $project_node->{$field_name}[LANGUAGE_NONE][0]['value'];
  }

  return $project_id;
}
