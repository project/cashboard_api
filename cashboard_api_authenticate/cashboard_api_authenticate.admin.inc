<?php

/**
 * cashboard_api_authenticate.admin.inc
 *
 * Settings forms definitions.
 */

/**
 * Defines a form to obtain the users cashboard credentials.
 */
function cashboard_api_authenticate_user_authenticate_form() {

  // Obtain user and store it in the form values.
  $account = menu_get_object('user');
  $form['user']['#type'] = 'value';
  $form['user']['#value'] = $account->uid;

  // Notify the user if they are already authenticated.
  // TODO: do an api call to check the credentials.
  if (isset($account->data['cashboard_api']['api_key'])) {
    drupal_set_message('An api key is currently stored for your user account.');
  }

  // Intro.
  $intro = t('Please enter your api credentials. Your password will not be saved. It is only used here to obtain your api key.');
  $form['intro']['#markdown'] = '<div>' . $intro . '</div>';

  // Cashboard subdomain.
  $form['subdomain'] = array(
    '#type' => 'textfield',
    '#title' => t('Cashboard subdomain'),
    '#description' => t('Your Cashboard subdomain. When logged into your Cashboard site this is visible in the url. For example if your logged in url has mysubdomain.cahsboardapp.com, "mysubdomain" is what you would enter here.'),
    '#default_value' => isset($account->data['cashboard_api']['subdomain']) ? $account->data['cashboard_api']['subdomain'] : '',
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  // Cashboard email address.
  $form['email_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Cashboard E-mail'),
    '#description' => t('The e-mail address you use to log into cashboard.'),
    '#default_value' => isset($account->data['cashboard_api']['email_address']) ? $account->data['cashboard_api']['email_address'] : '',
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  // Cashboard password.
  $form['password'] = array(
    '#type' => 'password',
    '#title' => t('Cashboard Password'),
    '#description' => t('The password you use to log into cashboard. This value will not be saved.'),
    '#default_value' => '',
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Authenticate'),
  );
  return $form;
}

/**
 * Form validation callback.
 */
function cashboard_api_authenticate_user_authenticate_form_validate($form, $form_state) {
  // Todo Validate e-mail and subdomain using regex. email is probably defined in core somewhere.
}

/**
 * Stores the users cashboard credentials and obtains the api key.
 */
function cashboard_api_authenticate_user_authenticate_form_submit($form, $form_state) {

  // First store the values that are safe to hold onto (and needed for api
  // communications).
  $account = user_load($form_state['values']['user']);
  $account->data['cashboard_api']['subdomain'] = $form_state['values']['subdomain'];
  $account->data['cashboard_api']['email_address'] = $form_state['values']['email_address'];

  // Obtain the api key.
  $api_key = cashboard_api_authenticate_obtain_api_key($form_state['values']['subdomain'], $form_state['values']['email_address'], $form_state['values']['password']);

  // If the api key is valid store it. Otherwise note the failure to the user.
  if ($api_key) {
    $account->data['cashboard_api']['api_key'] = $api_key;
    drupal_set_message('API key successfully obtained.');
  }
  else {
    // Reason for failure will dump into the watchdog form the api call.
    drupal_set_message(t('Failed to obtain API key.'));
  }

  // Username and e-mail are stored either way so user doesn't have to
  // enter them again on failure or api key reset.
  user_save($account, array('data' => $account->data));
}

/**
 * Form to obtain the cron user.
 *
 * TODO: Make this a user select list of users who have valid api credentials.
 */
function cashboard_api_authenticate_cron_user_form($form, $form_state) {
  $form['cashboard_api_cron_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Cron User ID'),
    '#description' => t('The uid of the user cron should act as. User must already have api credentials'),
    '#default_value' => variable_get('cashboard_api_cron_user', 1),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['cashboard_api_create_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Create User ID'),
    '#description' => t('The uid of the user create command should act as. User must already have api credentials'),
    '#default_value' => variable_get('cashboard_api_create_user', 1),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
