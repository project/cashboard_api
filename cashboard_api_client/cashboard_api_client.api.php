<?php

/**
 * Implements hook_cashboard_api_client_get_client_id().
 */
function hook_cashboard_api_client_get_client_id($entity_type, $entity) {
  $group = node_load($entity->og_group_ref[LANGUAGE_NONE][0]['target_id']);

  $field_list = cashboard_api_get_field_list('node', $group, 'cashboard_client');
  if (!empty($field_list)) {
    $field_name = array_shift($field_list);
    $client_id = $group->{$field_name}[LANGUAGE_NONE][0]['value'];
  }

  // 3. If still empty check client field on group.
  if (!isset($client_id)) {
    $client = node_load($group->field_client[LANGUAGE_NONE][0]['target_id']);
    $field_list = cashboard_api_get_field_list('node', $client, 'cashboard_client');

    if (!empty($field_list)) {
      $field_name = array_shift($field_list);
      $client_id = $client->{$field_name}[LANGUAGE_NONE][0]['value'];
    }
  }

  return $client_id;
}
