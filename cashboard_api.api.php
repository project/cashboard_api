<?php

/**
 * Implements hook_cashboard_api_info().
 */
function hook_cashboard_api_info() {
  $calls = array(
    'clients_list' => array( // The operation name that will be passed to the API call function.
      'path' => 'client_companies', // The path that goes after the domain
      'method' => 'get', // Options are get, post, path. Path is a form of get where data is appended to path instead of to the args array.
      'authorize' => TRUE, // Defaults to true but will check this value if it is set.
      'expected_status' => 'OK' // Defaults to OK but will check this value if it is set.
    ),
  );
  return $calls;
}

/**
 * Implements hook_cashboard_api_info_alter().
 *
 * Allows altering of api_info array.
 */
function hook_cashboard_api_info_alter(&$calls) {
  $calls['clients_list']['expected_status'] = 'created';
}
